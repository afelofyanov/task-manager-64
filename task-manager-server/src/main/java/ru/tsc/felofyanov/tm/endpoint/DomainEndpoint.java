package ru.tsc.felofyanov.tm.endpoint;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.felofyanov.tm.api.service.IDomainService;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;
import ru.tsc.felofyanov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.felofyanov.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    @Autowired
    private IDomainService domainService;

    @NotNull
    @Override
    @WebMethod
    public DataLoadBackupResponse loadDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadBackupRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataLoadBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBase64Response loadDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadBase64Request request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataLoadBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadBinaryResponse loadDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataLoadBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataLoadJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadJsonJaxbResponse loadDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadJsonJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataLoadJsonJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataLoadXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadXmlJaxbResponse loadDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadXmlJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataLoadXmlJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataLoadYamlFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataLoadYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataLoadYamlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBackupResponse saveDataBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveBackupRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataSaveBackupResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBase64Response saveDataBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveBase64Request request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataSaveBase64Response();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveBinaryResponse saveDataBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveBinaryRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataSaveBinaryResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveJsonFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataSaveJsonFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveJsonJaxbResponse saveDataJsonJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveJsonJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataSaveJsonJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveXmlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataSaveXmlFasterXmlResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveXmlJaxbResponse saveDataXmlJaxb(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveXmlJaxbRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataSaveXmlJaxbResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataSaveYamlFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataSaveYamlFasterXmlRequest request
    ) {
        check(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataSaveYamlFasterXmlResponse();
    }
}
