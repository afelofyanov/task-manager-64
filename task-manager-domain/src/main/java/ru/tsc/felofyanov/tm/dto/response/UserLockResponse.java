package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserLockResponse extends AbstractResultResponse {

    public UserLockResponse(@NotNull Throwable throwable) {
        super(throwable);
    }
}
